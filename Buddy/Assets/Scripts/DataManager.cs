﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class DataManager{

    public static string path = Application.persistentDataPath + "/progress.data";
    
    public static void SaveProgress (PlayerStatus SaveObj)
    {
        BinaryFormatter formatter = new BinaryFormatter();

        FileStream stream = new FileStream(DataManager.path, FileMode.Create);

        PlayerProgress data = new PlayerProgress(SaveObj);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static void DeleteProgress ()
    {
        if (File.Exists(path))
        {
            File.Delete(path);
        }
    }

    public static PlayerProgress LoadProgress()
    {
        if (File.Exists(path))
        {
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Open);
        
        PlayerProgress data = formatter.Deserialize(stream) as PlayerProgress;
        stream.Close();
        return data;
        }
        Debug.LogError("no file to load");
        return null; 

    }

}

//  PlayerData -> PlayerProgress
//  Player -> PlayerStatus