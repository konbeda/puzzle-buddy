﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using  df = defaultValues;
 
public static class PlayerBuilder
{

    public static void newPlayer(){
        Debug.Log("GENERATING BASIC STATUS"); 
       df.fome = df.maxF;
       df.sede = df.maxSe;
       df.sono = df.maxSo;
        int R = Random.Range(1,4);
        switch (R)
            {
                case 1:
                    df.arquetipo = "preguiçoso";
                    break;
                case 2:
                    df.arquetipo = "metodico";
                    break;
                case 3:
                    df.arquetipo = "agitado";
                    break;
            }
        setStatus();
    }
    public static void setStatus(){

        switch (df.arquetipo)
        {
            case "preguiçoso":
                // modificadores
                df.fomeMod = df.devagar;
                df.sedeMod = df.padrão;
                df.sonoMod = df.devagar;
                // prioridades
                df.fomePriority = 3;
                df.sedePriority = 1;
                df.sonoPriority = 2;
                // triggers
                df.fomeTrigger = df.alto;
                df.sedeTrigger = df.medio;
                df.sonoTrigger = df.baixo;
                break;

            case "metodico":
                // modificadores
                df.fomeMod = df.padrão;
                df.sedeMod = df.rapido;
                df.sonoMod = df.padrão;
                // prioridades
                df.fomePriority = 1;
                df.sedePriority = 3;
                df.sonoPriority = 2;
                // triggers
                df.fomeTrigger = df.medio;
                df.sedeTrigger = df.alto;
                df.sonoTrigger = df.medio;
                break;

            case "agitado":
                // modificadores
                df.fomeMod = df.devagar;
                df.sedeMod = df.devagar;
                df.sonoMod = df.medio;
                // prioridades
                df.fomePriority = 2;
                df.sedePriority = 1;
                df.sonoPriority = 3;
                // triggers
                df.fomeTrigger = df.baixo;
                df.sedeTrigger = df.baixo;
                df.sonoTrigger = df.medio;
                break;  
        }
    }
}
