﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerProgress
{
    public System.DateTime lastLogin;
    public float sono, fome, stress, sede;

    public PlayerProgress (PlayerStatus status)
    {
        sono = status.sono;
        fome = status.fome;
        stress = status.stress;
        sede = status.sede;
        lastLogin = System.DateTime.Now;
    }
}
