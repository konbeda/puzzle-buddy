﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MonoBehaviour
{
    public System.DateTime lastLogin;
    public float sono, fome, stress, sede;

    public void SavePlayer ()
    {
        DataManager.SaveProgress(this);
    }
    public void LoadProgress()
    {
       PlayerProgress data = DataManager.LoadProgress();

        sono = data.sono;
        fome = data.fome;
        stress = data.stress;
        sede = data.sede;
        lastLogin = data.lastLogin;
    }
}
