﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveObject
{
    public System.DateTime lastLogin;

    public SaveObject (SaveObject save){
        lastLogin = save.lastLogin;
    }
}
