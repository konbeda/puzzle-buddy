﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class capsuleFollow : MonoBehaviour
{
     public Camera cam;
     public Rigidbody capsule;
     public GameObject[] hudItens;
    void Start()
    {
        cam = cam.GetComponent<Camera>();
    }

        void Update()
    {
        Vector3 screenPos = cam.WorldToScreenPoint(capsule.position);
        this.transform.position = screenPos;
    }

    public void SetActiveButtons(){
        foreach (GameObject item in this.hudItens)
        {
            item.SetActive(!item.activeInHierarchy);
        }
    }
}
