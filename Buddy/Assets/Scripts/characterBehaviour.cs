﻿// using UnityEngine;
// using UnityEngine.AI;

// public class movementBehaviour : MonoBehaviour
// {
//     public float timer = 5.0f;
//     public float pSo;
//     public float pF;

//     public string [] objetosLiberados;
//     public string arquetipo;
//     public string statusName;
//     public int statusAtual;

//     public GameManager gm;
//     public GameObject [] ociosos;
//     // prioridades ---------------------------
//     public int fomeP;
//     public int sedeP;
//     public int sonoP;

//     //nescessidades --------------------------
//     public float fome;
//     public float sede;
//     public float sono;
//     public float temperatura;

//     // modificadores de nescessidades --------
//     public float fome_mod;
//     public float sede_mod;
//     public float sono_mod;
//     public float temperatura_mod;

//     // gatilhos ------------------------------
//     public int fome_trigger;
//     public int sede_trigger;
//     public int sono_trigger;
//     public int calor_trigger;
//     public int frio_trigger;

//     // nescessidades max ---------------------
//     public int maxF = 600;
//     public int maxSe = 100;
//     public int maxSo = 100;

//     //movimento ------------------------------
//     Transform destino;

//     NavMeshAgent corpo;
    
//     movementBehaviour MovBehav;
//     // ---------------------------------------

//     void Awake(){
//         corpo = this.GetComponent<NavMeshAgent>();
//         MovBehav = this.GetComponent<movementBehaviour>();
//         if(corpo == null){
//             Debug.LogError("corpo sem navmesh");
//         }
//     }

//     void Start()
//     {   
//         gm = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
//         if(arquetipo == ""){
//             gm.generateBasicStatus(MovBehav);
//         }
//         else{
//             gm.setStatus(arquetipo, MovBehav);
//         }
//     }

//     void Update()
//     {
//         timer -= Time.deltaTime;
//         pF = (fome/maxF) * 100;
//         pSo = (sono/maxSo) * 100;

//         if(timer <= 0.0f){
//             timer = 5.0f;
//             CheckStatus();
//         }

//     }
//     void CheckStatus(){
//         switch (statusName)
//         {
//             case "_fome": 
//                 if(pF>fome_trigger){
//                     statusName = "_ocioso";
//                     statusAtual = 0;
//                     StatusOcioso();
//                 };
//                 break;
//             case "_sono": 
//                 if(pSo>sono_trigger){
//                     statusName = "_ocioso";
//                     statusAtual = 0;
//                     StatusOcioso();
//                 };
//                 break;
//             case "_ocioso":
//                 StatusOcioso();
//                 break;
//             default:
//                 break;
//         }
//         if(pF<fome_trigger){
//             ChangeStatus(fomeP, "_fome");
//         }
//         if(pSo<sono_trigger){
//             ChangeStatus(sonoP, "_sono");
//         }
//     }
//     void StatusOcioso(){
//         ociosos = GameObject.FindGameObjectsWithTag("_ocioso");
//         int atual = Random.Range(0, ociosos.Length);
//         destino = ociosos[atual].transform;
//         SetDestination();
//     }

//     void ChangeStatus(int newStatus, string newStatusName){
//         if(statusAtual == 0 || statusAtual<newStatus){
//             statusAtual = newStatus;
//             statusName = newStatusName;
//             destino = GameObject.FindGameObjectWithTag(newStatusName).transform;
//             SetDestination();
//         }
//     }
//     public void SetDestination(){
//         if(destino != null){
//             Vector3 targetVector = destino.transform.position;
//             corpo.SetDestination(targetVector);
//         }
//     }
    
// }
