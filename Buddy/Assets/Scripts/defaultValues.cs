﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using  df = defaultValues;

public static class defaultValues 
{
    public static float timer = 5.0f;
    public static float pSo;
    public static float pF;
 
    public static string [] objetosLiberados;
    public static string arquetipo;
    public static string statusName;
    public static int statusAtual;

    // prioridades ---------------------------
    public static int fomePriority, sedePriority, sonoPriority;
 
    //nescessidades --------------------------
    public static float fome, sede, sono, temperatura;

    // mod ificadores de nescessidades --------
    public static float fomeMod, sedeMod, sonoMod, temperaturaMod;

    // gatilhos ------------------------------
    public static int fomeTrigger, sedeTrigger, sonoTrigger, calorTrigger, frioTrigger;

    // nescessidades max ---------------------
    public static int maxF = 600;
    public static int maxSe = 100;
    public static int maxSo = 100;

    public static float rapido = 1;
    public static float padrão = 1;
    public static float devagar = 1;
    public static int alto = 75;
    public static int medio = 45;
    public static int baixo = 15;
}