﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using df = defaultValues;

public class statusChanger : MonoBehaviour
{
   public GameObject Player;
   public PlayerStatus status;
    void Start()
    {
        Player = this.gameObject;
        status = Player.GetComponent<PlayerStatus>();
    }

    void Update()
    {
        status.sono =* df.sonoMod;
        status.fome =* df.fomeMod;
        status.stress =* df.stressMod;
        status.sede =* df.sedeMod;
    }
}