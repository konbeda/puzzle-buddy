﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class textToScreen : MonoBehaviour
{
private int necessityInt;
public float necessity;
private Slider _Slider;

private StreamReader reader;
private string csvContent;
public Text dotTxt;
private string Pth;

public bool sleeping = false;

    void Start()
    {
        _Slider = this.gameObject.GetComponent<Slider>();

        necessity = _Slider.maxValue;
    }

    void Update()
    {
        necessityInt = Mathf.FloorToInt(necessity);
        necessity -= Time.deltaTime;
    }
}
