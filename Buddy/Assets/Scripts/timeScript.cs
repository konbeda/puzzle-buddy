﻿using UnityEngine;

public class timeScript : MonoBehaviour
{
    public System.DateTime timeStamp, lastTimeStamp, checkZeroTime;
    public object diference;

    public void TimeStampFunction()
    {
        // valida se timeStamp está zerada, se confirmada, gera um valor atual para a variavel
        if(timeStamp == checkZeroTime){
        timeStamp =System.DateTime.Now;
        Debug.Log(timeStamp);
        Debug.Log("------------------------------------------");
        return;
        }

        // valida se timeStamp tem valor e se lastTimeStamp está zerado, se confirmado lastTimeStamp recebe o valor de timeStamp, e timeStamp recebe um novo valor atual
        if(timeStamp != checkZeroTime && lastTimeStamp == checkZeroTime){
        lastTimeStamp = timeStamp;
        timeStamp =System.DateTime.Now;
        Debug.Log(timeStamp);
        Debug.Log(lastTimeStamp);
        Debug.Log("------------------------------------------");
        return;
        }

        // computa a diferença de tempo entre o ultimo login e o horário atual, colocando o valor da diferença na variavel diference
        Debug.Log("------------------------------------------");
        diference = timeStamp.Subtract(lastTimeStamp);
        Debug.Log(diference);
    }
}